import React from 'react';
import { View, Text, ScrollView, Image, } from 'react-native';

const Story = () => {
    return (
        <View>
            <ScrollView horizontal={true}
                showsHorizontalScrollIndicator={false} style={{ height: 110, backgroundColor: 'white', paddingHorizontal: 15 }}>
                <Image source={require('../assets/8.jpg')}
                    style={{ height: 70, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../assets/9.jpg')}
                    style={{ height: 70, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../assets/10.jpg')}
                    style={{ height: 70, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../assets/11.jpg')}
                    style={{ height: 70, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../assets/12.jpg')}
                    style={{ height: 70, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../assets/13.jpg')}
                    style={{ height: 70, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
            </ScrollView>
        </View>
    )
}

export default Story
