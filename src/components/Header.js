import React from 'react'
import { View, TouchableOpacity, Image, StyleSheet, } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/Ionicons';


const Header = () => {
    return (
        <View style={Setting.navBar}>
            <Image source={require('../assets/1.png')} style={{ width: 130, height: 40 }} />
            <View style={Setting.righNav}>
                <TouchableOpacity>
                    <Icon style={Setting.NavItem} name="heart-outline" size={30} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon1 style={Setting.NavItem} name="chatbubbles-outline" size={30} />
                </TouchableOpacity>
            </View>
        </View>
    )
}


const Setting = StyleSheet.create({
    righNav: {
        flexDirection: 'row'
    },
    NavItem: {
        marginLeft: 20,
        marginVertical: 5
    },
    navBar: {
        height: 50,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-between',
    },
});

export default Header;