import Header from './Header';
import Story from './Story';
import Pages from './Pages';
import Footer from './Footer';

export { Header, Story, Pages, Footer };
